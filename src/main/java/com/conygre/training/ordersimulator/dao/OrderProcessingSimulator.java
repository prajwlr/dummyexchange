package com.conygre.training.ordersimulator.dao;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;
import java.util.Random;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Repository;

import com.conygre.training.ordersimulator.entities.PortfolioItem;
import com.conygre.training.ordersimulator.entities.Stocks;


/**
 * This class is a test harness class - it only exists for testing.
 * 
 * This is SIMULATING sending trade orders to an exchange for them to be filled.
 * 
 * This class uses the following codes to represent the state of an order:
 * 
 * 0 : initialized i.e. has not yet been processed at all
 * 1 : processing  i.e. the order has been sent to an exchange, we are waiting for a response
 * 2 : filled i.e. the order was successfully placed
 * 3 : rejected i.e. the order was not accepted by the trading exchange
 * 
 * The above are JUST SUGGESTED VALUES, you can change or improve as you see think is appropriate.
 */
@Repository
public class OrderProcessingSimulator {
	
	// Standard mechanism for logging with spring boot - org.slf4j is built-in to spring boot
	private static final Logger LOG = LoggerFactory.getLogger(OrderProcessingSimulator.class);

	// You'll need to change these to match whatever you called your table and status_code field
	// You may also need to change the database name in application-mysql.properties and application-h2.properties
	// The default database name that's used here is "appDB"
	private static final String TABLE = "stockdata";
	private static final String STATUS_CODE = "statusCode";

	private int percentFailures = 10;

	@Autowired
	private JdbcTemplate template;

	/**
	 * Any record in the configured database table with STATE=0 (init)
	 * 
	 * Will be changed to STATE=1 (processing)
	 */
	@Scheduled(fixedRateString = "${scheduleRateMs:10000}")
	public int findTradesForProcessing() {
		LOG.debug("In findTradesForProcessing !!");
		
		String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "=1 WHERE " + STATUS_CODE + "=0";
		int numberChanged = template.update(sql);
		
		LOG.debug("Updated [" + numberChanged + "] order from initialized (0) TO processing (1)");
		
		return numberChanged;
	}
	
	/**
	 * Anything in the configured database table with STATE=1 (processing)
	 * 
	 * Will be changed to STATE=2 (filled) OR STATE=3 (rejected)
	 * 
	 * This method uses a random number to determine when trades are rejected.
	 */
	@Scheduled(fixedRateString = "${scheduleRateMs:15000}")
	public int findTradesForFillingOrRejecting() {
		int totalChanged = 0;
		int lastChanged = 0;

		do {
			lastChanged = 0;
			
			int randomInteger = new Random().nextInt(100);

			LOG.debug("Random number is [" + randomInteger +
					  "] , failure rate is [" + percentFailures + "]");
			
			// use a random number to decide if we'll simulate success OR failure
			if(randomInteger > percentFailures) {
				// Mark this one as success
				lastChanged = markTradeAsSuccessOrFailure(2);
				LOG.debug("Updated [" + lastChanged + "] order from processing (1) TO success (2)");
			}
			else {
				// Mark this one as failure!!
				lastChanged = markTradeAsSuccessOrFailure(3);
				LOG.debug("Updated [" + lastChanged + "] order from processing (1) TO failure (3)");
			}
			totalChanged += lastChanged;

		} while (lastChanged > 0);

		return totalChanged;
	}

	/*
	 * Update a single record to success or failure
	 */
	public int markTradeAsSuccessOrFailure(int successOrFailure) {
		
		if(successOrFailure == 2) {
			return changePortfolio();
		}
//		String sql = "UPDATE " + TABLE + " SET " + STATUS_CODE + "= 3 " /*+
//	                 successOrFailure*/ + " WHERE " + STATUS_CODE + "=1 limit 1";
//		
//		return template.update(sql);
		return 0;
	}
	
	public int changePortfolio() {
		List<Stocks> stockList = getAllPendingStocks();
		
		for(Stocks stock : stockList) {
			LOG.debug("A stock found = " + stock.getStockTicker() + " : " + stock.getStatusCode());
			String sqlPortfolio = "SELECT datetime, stockTicker, price, volume, buyOrSell, statusCode FROM portfolio WHERE stockTicker=?";
			List<PortfolioItem> pfList = (List<PortfolioItem>) template.query(sqlPortfolio, new PortfolioItemRowMapper(), stock.getStockTicker());
			
			boolean isEmpty = true;
			if(pfList.size() > 0)isEmpty = false;
			
			if(stock.getBuyOrSell().equals("BUY")) {
				
				if(isEmpty ==true) {
					sqlPortfolio = "INSERT INTO portfolio(stockTicker, price, volume, buyOrSell, statusCode) "
							+ "VALUES(?,?,?,?,?)";
					template.update(sqlPortfolio, stock.getStockTicker(), stock.getPrice(), stock.getVolume(),
							stock.getBuyOrSell(), 2);
				}else {
					double resultPrice, oldPrice, newPrice;
					int resultVolume, oldVolume, newVolume;
					
					PortfolioItem pfItem = pfList.get(0);
					
					oldPrice = pfItem.getPrice();
					newPrice = stock.getPrice();
					
					oldVolume = pfItem.getVolume();
					newVolume = stock.getVolume();
					
					resultVolume = oldVolume + newVolume;
					resultPrice = ((oldPrice * oldVolume)+(newPrice*newVolume))/resultVolume;
					
					sqlPortfolio = "UPDATE portfolio SET stockTicker = ?, price = ?, volume = ?, buyOrSell = ?, statusCode = ? WHERE stockTicker = ?";
					template.update(sqlPortfolio, pfItem.getStockTicker(), resultPrice, resultVolume, pfItem.getBuyOrSell(),
							2, pfItem.getStockTicker());
					
				}
				String sql = "UPDATE stockdata SET statusCode = 2 WHERE id = ?" ;
				template.update(sql, stock.getId());
			}else {
				
				//For Sell Order!
				
				if(isEmpty == true) {
					LOG.debug("Cannot Sell the Stocks which are not present in the Portfolio");
					LOG.debug("Rejected!");
					String sql = "UPDATE stockdata SET statusCode = 3 WHERE id = ?" ;	
					template.update(sql, stock.getId());
				}else {
					double resultPrice, oldPrice, newPrice;
					int resultVolume, oldVolume, newVolume;
					
					PortfolioItem pfItem = pfList.get(0);
					
					oldPrice = pfItem.getPrice();
					newPrice = stock.getPrice();
					
					oldVolume = pfItem.getVolume();
					newVolume = stock.getVolume();
					
					if(newVolume > oldVolume) {
						LOG.debug("Cannot Sell more Stocks than owned!!");
						LOG.debug("Rejected!");
						String sql = "UPDATE stockdata SET statusCode = 4 WHERE id = ?" ;
						template.update(sql, stock.getId());
						break;
					}
					
					if(newVolume == oldVolume){
						sqlPortfolio = "DELETE FROM portfolio WHERE stockTicker = ?";
						template.update(sqlPortfolio, pfItem.getStockTicker());
						String sql = "UPDATE stockdata SET statusCode = 2 WHERE id = ?" ;
						template.update(sql, stock.getId());
						break;
					}
					
					resultVolume = oldVolume - newVolume;
					resultPrice = ((oldPrice * oldVolume)-(newPrice*newVolume))/resultVolume;
					
					sqlPortfolio = "UPDATE portfolio SET stockTicker = ?, price = ?, volume = ?, buyOrSell = ?, statusCode = ? WHERE stockTicker = ?";
					template.update(sqlPortfolio, pfItem.getStockTicker(), resultPrice, resultVolume, pfItem.getBuyOrSell(),
							2, pfItem.getStockTicker());
					String sql = "UPDATE stockdata SET statusCode = 2 WHERE id = ?" ;
					template.update(sql, stock.getId());
					
				}
//				String sql = "UPDATE stockdata SET statusCode = 2 WHERE id = ?" ;
//				template.update(sql, stock.getId());
				
				
			}
		}
		return stockList.size();
	}
	
	public List<Stocks> getAllPendingStocks(){
		String sql = "SELECT id, datetime, stockTicker, price, volume, buyOrSell, statusCode FROM stockdata where statusCode=1 limit 1";
		return (List<Stocks>) template.query(sql, new StocksRowMapper());
	}

	public void setPercentFailures(int percentFailures) {
		this.percentFailures = percentFailures;
	}
	
	class StocksRowMapper implements RowMapper<Stocks> {

		@Override
		public Stocks mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Stocks(rs.getInt("id"), rs.getString("stockTicker"), rs.getDouble("price"), rs.getInt("volume"),
					rs.getString("buyOrSell"), rs.getInt("statusCode"), rs.getString("datetime"));

		}

	}
	
	class PortfolioItemRowMapper implements RowMapper<PortfolioItem> {

		@Override
		public PortfolioItem mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new PortfolioItem( rs.getString("stockTicker"), rs.getDouble("price"), rs.getInt("volume"),
					rs.getString("buyOrSell"), rs.getInt("statusCode"), rs.getString("datetime"));

		}

	}

}