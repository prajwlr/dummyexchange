CREATE Database if not exists stocksdb;

use stocksdb;

create table stockdata(
id int not null primary key,
datetime timestamp default now() on update now(),
stockTicker varchar(10) not null,
price double not null,
volume int not null,
buyOrSell varchar(5) not null,
statusCode int not null
);

create table portfolio(
datetime timestamp default now() on update now(),
StockTicker varchar(10) not null primary key,
price double not null,
volume int not null,
buyOrSell varchar(5) not null,
statusCode int not null
);

INSERT INTO `stocksdb`.`stockdata` (`id`, `stockTicker`, `price`, `volume`, `buyOrSell`, `statusCode`) VALUES ('1', 'SBIN', '200', '10', 'BUY', '0');
INSERT INTO `stocksdb`.`stockdata` (`id`, `stockTicker`, `price`, `volume`, `buyOrSell`, `statusCode`) VALUES ('2', 'UBS', '500', '10', 'BUY', '0');
INSERT INTO `stocksdb`.`stockdata` (`id`, `stockTicker`, `price`, `volume`, `buyOrSell`, `statusCode`) VALUES ('3', 'Barclays', '400', '5', 'SELL', '0');
